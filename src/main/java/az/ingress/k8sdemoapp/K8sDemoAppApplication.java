package az.ingress.k8sdemoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K8sDemoAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(K8sDemoAppApplication.class, args);
    }

}
